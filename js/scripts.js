
"use strict";

/* ------------------------------------------------------------ *\
|* ------------------------------------------------------------ *|
|* Classie.js
|* https://github.com/desandro/classie/blob/master/classie.js
|* ------------------------------------------------------------ *|
\* ------------------------------------------------------------ */
( function( window ) {

// class helper functions from bonzo https://github.com/ded/bonzo

function classReg( className ) {
  return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
}

// classList support for class management
// altho to be fair, the api sucks because it won't accept multiple classes at once
var hasClass, addClass, removeClass;

if ( 'classList' in document.documentElement ) {
  hasClass = function( elem, c ) {
    return elem.classList.contains( c );
  };
  addClass = function( elem, c ) {
    elem.classList.add( c );
  };
  removeClass = function( elem, c ) {
    elem.classList.remove( c );
  };
}
else {
  hasClass = function( elem, c ) {
    return classReg( c ).test( elem.className );
  };
  addClass = function( elem, c ) {
    if ( !hasClass( elem, c ) ) {
      elem.className = elem.className + ' ' + c;
    }
  };
  removeClass = function( elem, c ) {
    elem.className = elem.className.replace( classReg( c ), ' ' );
  };
}

function toggleClass( elem, c ) {
  var fn = hasClass( elem, c ) ? removeClass : addClass;
  fn( elem, c );
}

var classie = {
  // full names
  hasClass: hasClass,
  addClass: addClass,
  removeClass: removeClass,
  toggleClass: toggleClass,
  // short names
  has: hasClass,
  add: addClass,
  remove: removeClass,
  toggle: toggleClass
};

// transport
if ( typeof define === 'function' && define.amd ) {
  // AMD
  define( classie );
} else {
  // browser global
  window.classie = classie;
}

})( window );

// expanding search
// http://callmenick.com/post/expanding-search-bar-using-css-transitions

(function(window){

  // get vars
  var searchEl = document.querySelector("#input");
  var labelEl = document.querySelector("#label");

  // register clicks and toggle classes
  labelEl.addEventListener("click",function(){
    if (classie.has(searchEl,"focus")) {
      classie.remove(searchEl,"focus");
      classie.remove(labelEl,"active");
    } else {
      classie.add(searchEl,"focus");
      classie.add(labelEl,"active");
    }
  });

  // register clicks outisde search box, and toggle correct classes
  document.addEventListener("click",function(e){
    var clickedID = e.target.id;
    if (clickedID != "search-terms" && clickedID != "search-label") {
      if (classie.has(searchEl,"focus")) {
        classie.remove(searchEl,"focus");
        classie.remove(labelEl,"active");
      }
    }
  });
}(window));

// http://www.jqueryscript.net/menu/Simple-Multi-level-Off-canvas-Menu-Plugin-jQuery-Essence-Drawermenu.html
$('.drawermenu').drawermenu({
  speed:100,
  position:'left'
});

// footer links turn into accordion
jQuery("h5").click(function(){
  jQuery(this).parent(".km-footer_col").toggleClass("open"); 
  jQuery('html, body').animate({ scrollTop: jQuery(this).offset().top - 170 }, 500 );
}); 

// Animated Fixed Header (Scroll Down)

$(function(){
 var shrinkHeader = 300;
  $(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= shrinkHeader ) {
           $('header').addClass('shrink');
        }
        else {
            $('header').removeClass('shrink');
        }
  });
function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
    }
});

// Global Carousels

$('.km-slr_carousel').owlCarousel({
    loop:true,
    dots:false,
    margin:0,
    nav:true,
    responsiveclass:false,
    navText: ['<img src="img/left.svg">', '<img src="img/right.svg">'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        },
        1200:{
            items:3
        }
    }
})

$('.km-newar_carousel').owlCarousel({
    loop:true,
    dots:false,
    margin:0,
    nav:true,
    autoplay:true,
    navText: ['<img src="img/left.svg">', '<img src="img/right.svg">'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
    }
})


$('.km-slr-logo_carousel').owlCarousel({
    loop:true,
    margin:10,
    dots:false,
    nav:false,
    stagePadding: 50,
    margin:30,
    autoplay:true,
    center:true,
    autoWidth:false,
    responsive:{
        0:{
            items:2,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:6,
            nav:false,
        },
    }
})

// megamenu add active class

$(".nav-item").hover(
  function () {
    $(this).toggleClass("active");
  }
);




